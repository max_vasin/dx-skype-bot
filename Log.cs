﻿using System;

namespace DevExpress.SkypeBot {
    public static class Log {
        public static void Normal(string format, params object[] args) {
            Console.Out.WriteLine(format, args);
        }
        public static void Warning(string format, params object[] args) {
            Console.Out.WriteLine(format, args);
        }
        public static void Error(string format, params object[] args) {
            Console.Error.WriteLine(format, args);
        }
    }
}
