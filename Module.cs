﻿using Interop.SKYPE4COMLib;
namespace DevExpress.SkypeBot {
    public abstract class Module {
        public abstract string Command { get; }
        public abstract string Description { get; }

        public abstract void SkypeAttached(Skype skype);
        public abstract void SkypeWillDetached(Skype skype);
        public abstract void SkypeMessage(ChatMessage pMessage, TChatMessageStatus Status);
    }
}
