﻿using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Linq;
using System;
using Interop.SKYPE4COMLib;

namespace DevExpress.SkypeBot {
    class ModuleManager {
        public const string commandPrefix = "#";
        static string[] searchPaths = { "modules" };
        static ModuleManager defaultManager;
        
        public static ModuleManager DefaultManager {
            get {
                if (defaultManager == null)
                    defaultManager = new ModuleManager();
                return defaultManager;
            }
        }

        readonly Dictionary<string, Module> modules;

        public Module[] Modules {
            get { return modules.Values.ToArray();  }
        }

        ModuleManager() {
            this.modules = new Dictionary<string, Module>();
            LoadModules();
        }

        void LoadAssembly(Assembly asm) {
            foreach (Type type in asm.ExportedTypes) {
                if (!type.IsAbstract && type.IsSubclassOf(typeof(Module))) {
                    Module module = (Module)asm.CreateInstance(type.FullName);
                    
                    modules.Add(module.Command, module);
                }
            }
        }
        void LoadModules() {
            Log.Normal("Loading modules");

            LoadAssembly(GetType().Assembly);

            foreach (string path in searchPaths) {
                string fullPath = Path.GetFullPath(path);

                if (Directory.Exists(fullPath)) {
                    var files = Directory.EnumerateFiles(fullPath, "*.*", SearchOption.AllDirectories).Where(s => s.EndsWith(".dll") || s.EndsWith(".exe"));

                    foreach (string file in files) {
                        try {
                            LoadAssembly(Assembly.LoadFile(file));
                        } catch (Exception exc) {
                            Log.Error("Modules loading exception:");
                            Log.Error(exc.ToString());
                        }
                    }
                }
            }

            Log.Normal("[{0}] modules loaded", modules.Count);
        }

        public void SkypeAttached(Skype skype) {
            foreach (Module module in modules.Values)
                module.SkypeAttached(skype);
        }
        public void SkypeWillDetach(Skype skype) {
            foreach (Module module in modules.Values)
                module.SkypeWillDetached(skype);
        }
        public void SkypeMessage(ChatMessage pMessage, TChatMessageStatus Status) {
            string body = pMessage.Body.Trim();
            
            if (body.StartsWith(commandPrefix)) {
                body = body.Substring(1);

                if (modules.ContainsKey(body)) {
                    Module module = modules[body];
                    module.SkypeMessage(pMessage, Status);
                }
            }
        }
    }
}
