﻿using Interop.SKYPE4COMLib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace DevExpress.SkypeBot.Modules {
    public struct InfoMoney {
        public string Symbol;
        public double Bid;
        public double Ask;
        public double Change;
        public int Digits;
        public DateTime Lasttime;

        public double Value { get { return Math.Round(((Ask + Bid) / 2.0), Digits); } }
    }

    public class CurrencyModule : Module {
        double oldUSD = 0, oldEUR = 0, oldEUR_USD = 0;
        const string upStr = "▲";
        const string downStr = "▼"; 
        
        public override string Command {
            get { return "currency"; }
        }

        public override string Description {
            get { return "чо там с баксом"; }
        }

        string GetCurrencyXmlData() {
            string url = @"https://quotes.instaforex.com/get_quotes.php?q=eurusd,usdrur";
            string xml = string.Empty;

            try {
                HttpWebRequest myHttpWebRequest = (HttpWebRequest)HttpWebRequest.Create(url);
                HttpWebResponse myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();
                StreamReader myStreamReader = new StreamReader(myHttpWebResponse.GetResponseStream());
                xml = myStreamReader.ReadToEnd();
            } catch (Exception e) {
                return null;
            }

            return xml;
        }
        public InfoMoney Xml2InfoMoney(XmlReader reader) {
            InfoMoney inf = new InfoMoney();
            while (reader.Read() && reader.Name != "item") {
                if (reader.NodeType == XmlNodeType.Element) {
                    switch (reader.Name) {
                        case "symbol":
                            reader.Read();
                            inf.Symbol = reader.Value;
                            break;
                        case "bid":
                            reader.Read();
                            inf.Bid = double.Parse(reader.Value);
                            break;
                        case "ask":
                            reader.Read();
                            inf.Ask = double.Parse(reader.Value);
                            break;
                        case "change":
                            reader.Read();
                            inf.Change = double.Parse(reader.Value);
                            break;
                        case "digits":
                            reader.Read();
                            string strValue = reader.Value;
                            inf.Digits = string.IsNullOrEmpty(strValue) ? 0 : int.Parse(reader.Value);
                            break;
                        case "lasttime":
                            reader.Read();
                            //inf.Lasttime = DateTime.FromBinary(long.Parse(reader.Value));
                            break;
                    }
                }
            }
            return inf;
        }
        InfoMoney CalculateEURRUR(Dictionary<string, InfoMoney> currencies) {
            int digit = currencies["EURUSD"].Digits;
            double eurusdAsk = currencies["EURUSD"].Ask;
            double usdrurAsk = currencies["USDRUR"].Ask;

            double eurusdBid = currencies["EURUSD"].Bid;
            double usdrurBid = currencies["USDRUR"].Bid;

            InfoMoney inf = new InfoMoney();
            inf.Symbol = "EURRUR";
            inf.Ask = Math.Round(usdrurAsk * eurusdAsk, digit);
            inf.Bid = Math.Round(usdrurBid * eurusdBid, digit);
            inf.Digits = digit;

            return inf;
        }

        public override void SkypeAttached(Skype skype) { }
        public override void SkypeWillDetached(Skype skype) { }
        public override void SkypeMessage(ChatMessage pMessage, TChatMessageStatus Status) {
            string xml = GetCurrencyXmlData();

            IChat chat = pMessage.Chat;

            if (string.IsNullOrEmpty(xml)) {
                chat.SendMessage("Беда, парни. Эксепшн внутри меня где-то похоже :(");
                return;
            }

            try {
                Dictionary<string, InfoMoney> currencies = new Dictionary<string, InfoMoney>();
                using (XmlReader reader = XmlReader.Create(new StringReader(xml))) {
                    while (reader.Read()) {
                        if (reader.NodeType == XmlNodeType.Element && reader.Name == "item") {
                            InfoMoney inf = Xml2InfoMoney(reader);
                            currencies.Add(inf.Symbol, inf);
                        }
                    }
                }

                InfoMoney eurrurInf = CalculateEURRUR(currencies);
                currencies.Add(eurrurInf.Symbol, eurrurInf);

                double USD = currencies["USDRUR"].Value;
                double EUR = currencies["EURRUR"].Value;
                double EUR_USD = currencies["EURUSD"].Value;
                string format = "{1} {0} {2}    {4} {3} {5}    {7} {6} {8}";
                if (oldUSD == 0.0) {
                    format = "{1} : {2}    {4} : {5}    {7} : {8}";
                }
                string str = String.Format(format, USD > oldUSD ? upStr : downStr,
                currencies["USDRUR"].Symbol, USD,
                EUR > oldEUR ? upStr : downStr,
                currencies["EURRUR"].Symbol, EUR,
                EUR_USD > oldEUR_USD ? upStr : downStr,
                currencies["EURUSD"].Symbol, EUR_USD);

                if (oldUSD > 0.0)
                    chat.SendMessage(str);
                else
                    chat.SendMessage(String.Format("{0}, чел, сорь, не готов ответить, спроси еще раз", pMessage.Sender.FullName));

                oldUSD = currencies["USDRUR"].Value;
                oldEUR = currencies["EURRUR"].Value;
                oldEUR_USD = currencies["EURUSD"].Value;
            } catch (Exception exc) {
                chat.SendMessage("Проклятие! Я - скрешился :(\n" + exc.ToString());
            }

        }
    }
}
