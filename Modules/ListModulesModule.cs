﻿using Interop.SKYPE4COMLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevExpress.SkypeBot.Modules {
    public class ListModulesModule : Module {
        static string[] answers = {
            "Сейчас покажу тупому {0} все команды",
            "{0} ну ты чо? Запомни уже",
            "{0}, еще один, блин",
            "{0}, тебе заняться больше нечем? Ок, смотри"
        };
        
        public override string Command {
            get { return "list"; }
        }

        public override string Description {
            get { return "покажет тебе все доступные команды"; }
        }
        
        public override void SkypeAttached(Skype skype) { }
        public override void SkypeWillDetached(Skype skype) { }
        public override void SkypeMessage(ChatMessage pMessage, TChatMessageStatus Status) {
            string message = String.Format(Utils.RandomString(answers) + ":\n", pMessage.Sender.FullName);

            foreach (Module module in ModuleManager.DefaultManager.Modules) {
                message += ModuleManager.commandPrefix + module.Command + ((module == this) ? " сейчас на это смотришь\n" : " " + module.Description + "\n");
            }

            IChat chat = pMessage.Chat;
            chat.SendMessage(message);
        }
    }
}
