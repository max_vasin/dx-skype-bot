﻿using Interop.SKYPE4COMLib;
using System;
namespace DevExpress.SkypeBot {
    class Program {
        static ModuleManager moduleManager;

        static void Main(string[] args) {
            moduleManager = ModuleManager.DefaultManager;
            Skype skype = new Skype();

            skype.Attach();
            moduleManager.SkypeAttached(skype);

            skype.MessageStatus += OnMessageStatus;
            
            
            Log.Normal("Avail chats");
            foreach (IChat chat in skype.Chats)
                Log.Normal(chat.Name);

            Log.Normal("Recent chats");
            foreach (IChat chat in skype.RecentChats)
                Log.Normal(chat.Name);

            while (true) {
                
            }
        }

        static void OnMessageStatus(ChatMessage pMessage, TChatMessageStatus Status) {
            IChat chat = pMessage.Chat;
            
            try {
                if (chat != null)
                    Log.Normal("Rcvd msg in chat: {0} with topic {1}", chat.Name, "");
                else
                    Log.Normal("Chat is NULL");
            } catch (Exception exc) {
                Log.Error(exc.ToString());
            }
            
            if (Status != TChatMessageStatus.cmsSent)
                moduleManager.SkypeMessage(pMessage, Status);
        }
    }
}
