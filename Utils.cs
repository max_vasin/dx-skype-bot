﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevExpress.SkypeBot {
    public static class Utils {
        static Random rand = new Random((int)DateTime.Now.Ticks);
        public static string RandomString(string[] strings) {
            int index = rand.Next(strings.Length - 1);
            return strings[index];
        }
    }
}
